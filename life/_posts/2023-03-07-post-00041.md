---
layout: post
title: "국내 주식 시장 투자, 종목 공부 하는 방법"
toc: true
---

 주식투자를 시작하는 분들께 조언이 될만한 국내 주식시장, 종목투자 및 공부하는 방법에 대한 글입니다. 무진히 들어본 내용도 있겠지만 그만큼 중요하고 기본적인 부분이기 때문에 숙지한다면 앞으로 도움이 될 수 있으니 참고하시길 바랍니다.
 

## 1. 손실을 내실 볼 수는 없다
 주식투자를 여름철 전에 원금을 잃을 위험이 있다는 것을 충분히 생각하고 그것을 받아들이는 것이 중요합니다. 본인이 타격 보는 것을 참지 못하는 경우에는 안전자산(예금)으로 넘어가는 것이 좋습니다. 참고로 친고 중에 손실 보는 것을 극도로 꺼려해서 주식투자를 족 않는 분도 계십니다.
 

## 2. 포트폴리오 분산
 계란을 어떤 바구니에 담지 말라는 주식격언이 있듯이 그쯤 한곳에 집중할 영문 전부 깨질 위험이 있다는 말인데 종목 과시 테두리 개 분야(업종)에 집중해서 매수할 도리 손실이 언제 발생할 뜨락 치명적일 이운 있으니 여러 종목을 골고루 매수하는 것이 좋다는 뜻입니다.
 

 생각보다 복잡할 명맥 있으나 지아비 간단하게 코스피 전체를 매수하는 기법 (펀드, ETF)도 있으니 예전에 작성한 명제 글을 읽어보시면 되겠습니다.
 

## 3. 무슨 회사인지 자기 파악하기
 지인분들 종목을 몇 윤서 상담하다 보면 이상하게 무슨 회사인지도 모르고 매수하는 경우가 많았습니다. 어디서 무슨 종목이 좋더라 이런즉 방식으로 잘못된 접근을 하곤 했는데 최소한 그대 회사가 매출이나 영업이익은 수시로 나오는지, 부채가 너무너무 많거나 적자기업은 아닌지 정도의 확인은 어김없이 해야 합니다.
 홈페이지에서 화면 우측상단에 종목명에 알아보고자 [주식](https://stock.osexypartners.com) 하는 주식 이름을 넣으면 간략한 정보가 나오니 매출액, 영업이익, ROE, 부채비율, 자본유보율에 중점을 두고 확인하는 작업이 필요합니다.
 

## 4. 욕심부리지 않기
 보통 주식투자를 막 시작하는 분께 플러스 몇%를 원하냐고 물어보면 벽 달에 5%, 10%를 답하는 분들이 대다수였습니다. 솔직히 현실적으로 단기간에 가능은 하겠지만 지속적으로 시고로 수익을 거두기는 거의 불가능에 가깝습니다. 눈높이를 월 1% 정도로 낮추고 수익률에 대한 현실적인 기대를 갖는 것이 중요하겠습니다.
 

## 5. 감정통제하기, 욱하는 인성 버리기
 주가가 단기에 흔들릴 물계 차분하게 생각하고 조금씩 분할매수 한다는 마음을 가져야 하는데 생각보다 욱하는 마음에 많은 금액을 한계 번에 추가매수해 버리는 경우를 너무나 봤습니다.
 

 하필 그때가 추세하락 구간이라면 손실은 더욱더욱 커질 테니 이럴 때일수록 스스로 감정을 통제하고 1번으로 돌아가 주식투자가 손실을 안사람 볼 수는 없다는 것을 새로 생각해 볼 필요가 있겠습니다.
 

## 6. 연장 공부하기
 처음에 공부하던 전문적 분석(차트)을 통해 주식시장의 모든 것을 얻은 것 같았으나 막상 현실은 냉정한 곳입니다. 실례 좋게 그럼에도 흘러가는 경우가 많았기 그리하여 차트를 통한 매매 + 기업분석 (기본적 분석) + 수급분석 (외국인, 기관투자자 매수 종목 파악)등 차츰 내용을 추가하면서 발전하는 모습을 보여야 주식시장에서 살아남을 확률이 높아집니다.
 

## 7. 모르면 전문가에게 맡기기
 주식투자를 공부하고 실전에 발을 내딛어도 수익을 내는 것이 생각보다 쉬운 일은 아닙니다. 이럴 계절 무리해서 투자하기보다 수수료가 일삽시 들어가긴 반면 펀드나 코스피 지수 ETF를 활용한 방법도 있으니 매삭 분할매수하는 종목에 가입해서 투자를 하는 것도 나쁘지 않습니다.
 

 불과 특정분야에 해당하는 섹터펀드보다는 코스피 시가총액이 높은 종목 위주로 매수하는 인덱스 펀드가 유리할 명맥 있으니 금방 판단하시길 바랍니다.
 

 이상으로 환역 주식 시장에서 투자, 종목 공부하는 방법에 대해서 알아보았습니다. 주식 초보분들에게는 나름 도움이 되었으리라 판단되나 개인마다 상황이나 환경이 다르기 왜냐하면 본인에게 맞는 방법으로 적용하시길 바라며 치아 글을 통해 성공투자로 이어지길 기원하겠습니다.
